package fi.fmi.avi.demo.airmetdemo;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fi.fmi.avi.converter.AviMessageConverter;
import fi.fmi.avi.converter.ConversionIssue;
import fi.fmi.avi.converter.ConversionResult;
import fi.fmi.avi.converter.iwxxm.conf.IWXXMConverter;
import fi.fmi.avi.converter.tac.conf.TACConverter;
import fi.fmi.avi.demo.ConverterConfiguration;
import fi.fmi.avi.model.PartialOrCompleteTimeInstant;
import fi.fmi.avi.model.PartialOrCompleteTimePeriod;
import fi.fmi.avi.model.PhenomenonGeometryWithHeight;
import fi.fmi.avi.model.UnitPropertyGroup;
import fi.fmi.avi.model.immutable.PhenomenonGeometryWithHeightImpl;
import fi.fmi.avi.model.immutable.UnitPropertyGroupImpl;
import fi.fmi.avi.model.immutable.PhenomenonGeometryWithHeightImpl.Builder;
import fi.fmi.avi.model.sigmet.AIRMET;
import fi.fmi.avi.model.sigmet.immutable.AIRMETImpl;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(ConverterConfiguration.class)
class AirmetDemo implements CommandLineRunner {
    @Autowired
    AviMessageConverter airmetAviMessageConverter;

    public void run(String ...args) {
        System.out.println("RUN AIRMET");
        convertAirmetToIwxxm();
    }

    public void convertAirmetToIwxxm() {
        String fn = "airmet.tac";
        String input="";
        try {
            input = new String(Files.readAllBytes(Paths.get(fn)));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        System.out.println("input: ["+ input+"]");

        ConversionResult<AIRMET> result = airmetAviMessageConverter.convertMessage(input, TACConverter.TAC_TO_AIRMET_POJO);
        System.out.println("ran TAC "+result.getStatus());
        if (ConversionResult.Status.SUCCESS == result.getStatus()) {
            Optional<AIRMET> pojo = result.getConvertedMessage();
            if (pojo.isPresent()) {
                AIRMET airmet = pojo.get();
                AIRMETImpl.Builder builder = AIRMETImpl.immutableCopyOf(airmet).toBuilder();

                builder.setTranslated(false);

                ZonedDateTime reference_time = ZonedDateTime.now();
                //Fix issueTime
                PartialOrCompleteTimeInstant issueTime;
                if (airmet.getIssueTime().isPresent()) {
                    issueTime = airmet.getIssueTime().get();
                } else {
                    issueTime = PartialOrCompleteTimeInstant.of(reference_time);
                }
                PartialOrCompleteTimeInstant.Builder tb = PartialOrCompleteTimeInstant.builder().mergeFrom(issueTime);
                tb.completePartialNear(reference_time);
                builder.setIssueTime(tb.build());

                //Fix validityPeriod
                PartialOrCompleteTimePeriod validityPeriod = airmet.getValidityPeriod();
                PartialOrCompleteTimePeriod.Builder validityPeriodBuilder = PartialOrCompleteTimePeriod.builder().mergeFrom(validityPeriod);
                validityPeriodBuilder.completePartialEndingNear(reference_time)
                .completePartialStartingNear(reference_time);
                builder.setValidityPeriod(validityPeriodBuilder.build());

                //Fix analysisTimes
                if (airmet.getAnalysisGeometries().isPresent()) {
                    List<PhenomenonGeometryWithHeight>analysisGeometries = new ArrayList<>();
                    for (final PhenomenonGeometryWithHeight geometryWithHeight : airmet.getAnalysisGeometries().get()) {
                        Builder phenomenonGeometryWithHeightBuilder = PhenomenonGeometryWithHeightImpl.Builder.from(geometryWithHeight);
                        if (geometryWithHeight.getTime().isPresent()) {
                            PartialOrCompleteTimeInstant.Builder tb1 = PartialOrCompleteTimeInstant.builder().mergeFrom(geometryWithHeight.getTime().get());
                            tb1.completePartialNear(builder.getValidityPeriodBuilder().build().getStartTime().get().getCompleteTime().get());
                            phenomenonGeometryWithHeightBuilder.setTime(tb1.build());
                        }
                        analysisGeometries.add(phenomenonGeometryWithHeightBuilder.build());
                    }
                    builder.setAnalysisGeometries(analysisGeometries);
                }
                //Fix MWO to upper case
                UnitPropertyGroup mwo = builder.getMeteorologicalWatchOffice();
                UnitPropertyGroupImpl.Builder mwoBuilder = new UnitPropertyGroupImpl.Builder();
                mwoBuilder.setPropertyGroup(mwo.getName().toUpperCase(), mwo.getDesignator(), mwo.getType());
                builder.setMeteorologicalWatchOffice(mwoBuilder.build());

                AIRMET timeCorrectedAirmet = builder.build();

                ConversionResult<String> iwxxmResult = airmetAviMessageConverter.convertMessage(timeCorrectedAirmet, IWXXMConverter.AIRMET_POJO_TO_IWXXM30_STRING, null);
                if  (ConversionResult.Status.SUCCESS == iwxxmResult.getStatus()) {
                    System.out.println(iwxxmResult.getConvertedMessage().get());
                } else {
                    System.out.println("IWXXM failed");
                    System.err.println(iwxxmResult.getConversionIssues().size());
                    for (ConversionIssue ci: iwxxmResult.getConversionIssues()) {
                        System.out.println(ci);
                    }
                }
            } else {
                System.err.println("no pojo");
            }
        } else {
            System.err.println(result.getConversionIssues().size());
            for (ConversionIssue ci: result.getConversionIssues()) {
                System.out.println(ci);
            }
        }
    }

    private static Logger LOG = LoggerFactory.getLogger(AirmetDemo.class);

    public static void main(String[] args){
        LOG.info("START AIRMET");
        SpringApplication.run(AirmetDemo.class, args);
    }


}