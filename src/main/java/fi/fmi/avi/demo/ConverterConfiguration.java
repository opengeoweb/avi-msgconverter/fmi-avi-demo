package fi.fmi.avi.demo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import fi.fmi.avi.converter.AviMessageConverter;
import fi.fmi.avi.converter.AviMessageSpecificConverter;
import fi.fmi.avi.converter.iwxxm.conf.IWXXMConverter;
import fi.fmi.avi.converter.tac.conf.TACConverter;
import fi.fmi.avi.model.sigmet.AIRMET;
import fi.fmi.avi.model.sigmet.SIGMET;
import fi.fmi.avi.model.taf.TAF;
import fi.fmi.avi.model.metar.METAR;

@Configuration
@Import(value = {fi.fmi.avi.converter.tac.conf.TACConverter.class,fi.fmi.avi.converter.iwxxm.conf.IWXXMConverter.class })
public class ConverterConfiguration {
    @Autowired
    private AviMessageSpecificConverter<String, TAF> tafTACParser;

    @Autowired
    private AviMessageSpecificConverter<TAF, String> tafIWXXM30StringSerializer;

    @Autowired
    private AviMessageSpecificConverter<String, SIGMET> sigmetTACParser;

    @Autowired
    private AviMessageSpecificConverter<SIGMET, String> sigmetIWXXM30StringSerializer;

    @Autowired
    private AviMessageSpecificConverter<String, AIRMET> airmetTACParser;

    @Autowired
    private AviMessageSpecificConverter<AIRMET, String> airmetIWXXM30StringSerializer;

    @Autowired
    private AviMessageSpecificConverter<String, METAR> metarTACParser;

    // @Autowired
    // private AviMessageSpecificConverter<METAR, String> metarIWXXM30StringSerializer;

    @Bean
    public AviMessageConverter tafAviMessageConverter() {
        final AviMessageConverter converter = new AviMessageConverter();
        converter.setMessageSpecificConverter(TACConverter.TAC_TO_TAF_POJO, tafTACParser);
        converter.setMessageSpecificConverter(IWXXMConverter.TAF_POJO_TO_IWXXM30_STRING, tafIWXXM30StringSerializer);
        return converter;
    }

    @Bean
    public AviMessageConverter sigmetAviMessageConverter() {
        final AviMessageConverter converter = new AviMessageConverter();
        converter.setMessageSpecificConverter(TACConverter.TAC_TO_SIGMET_POJO, sigmetTACParser);
        converter.setMessageSpecificConverter(IWXXMConverter.SIGMET_POJO_TO_IWXXM30_STRING, sigmetIWXXM30StringSerializer);
        return converter;
    }

    @Bean
    public AviMessageConverter airmetAviMessageConverter() {
        final AviMessageConverter converter = new AviMessageConverter();
        converter.setMessageSpecificConverter(TACConverter.TAC_TO_AIRMET_POJO, airmetTACParser);
        converter.setMessageSpecificConverter(IWXXMConverter.AIRMET_POJO_TO_IWXXM30_STRING, airmetIWXXM30StringSerializer);
        return converter;
    }

    @Bean
    public AviMessageConverter metarAviMessageConverter() {
        final AviMessageConverter converter = new AviMessageConverter();
        converter.setMessageSpecificConverter(TACConverter.TAC_TO_METAR_POJO, metarTACParser);
        // converter.setMessageSpecificConverter(IWXXMConverter.METAR_POJO_TO_IWXXM30_STRING, metarIWXXM30StringSerializer);
        return converter;
    }
}
