package fi.fmi.avi.demo.metardemo;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.ZonedDateTime;
import java.util.Optional;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fi.fmi.avi.converter.AviMessageConverter;
import fi.fmi.avi.converter.ConversionIssue;
import fi.fmi.avi.converter.ConversionResult;
import fi.fmi.avi.converter.tac.conf.TACConverter;
import fi.fmi.avi.demo.ConverterConfiguration;
import fi.fmi.avi.model.Aerodrome;
import fi.fmi.avi.model.AviationCodeListUser.PermissibleUsage;
import fi.fmi.avi.model.immutable.AerodromeImpl;
import fi.fmi.avi.model.immutable.CoordinateReferenceSystemImpl;
import fi.fmi.avi.model.immutable.ElevatedPointImpl;
import fi.fmi.avi.model.metar.METAR;
import fi.fmi.avi.model.metar.immutable.METARImpl;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(ConverterConfiguration.class)
class MetarDemo implements CommandLineRunner {
    @Autowired
    AviMessageConverter metarAviMessageConverter;

    public void run(String ...args) {
        System.out.println("RUN METAR");
        convertMetarToIwxxm();
    }

    public void convertMetarToIwxxm() {
        String fn = "metar.tac";
        String input="";
        try {
            input = new String(Files.readAllBytes(Paths.get(fn)));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        System.out.println("input: ["+ input+"]");

        ConversionResult<METAR> result = metarAviMessageConverter.convertMessage(input, TACConverter.TAC_TO_METAR_POJO);
        System.out.println("ran TAC "+result.getStatus());
        if  (ConversionResult.Status.SUCCESS == result.getStatus()) {
            Optional<METAR> pojo = result.getConvertedMessage();
            if (pojo.isPresent()) {
                METAR metar = pojo.get();
                METARImpl.Builder builder = METARImpl.immutableCopyOf(metar).toBuilder();
                Aerodrome ad = AerodromeImpl.Builder.from(metar.getAerodrome())
                .setName("airport name")
                .setReferencePoint(ElevatedPointImpl.builder()
                        .setCrs(CoordinateReferenceSystemImpl.wgs84())
                        .addCoordinates(24.8325, 59.413333)
                        .setElevationUom("M")
                        .setElevationValue(999.)
                        .setVerticalDatum("EGM_96")
                        .build())
                .setLocationIndicatorICAO("EHXX")
                .build();
                builder = builder.setAerodrome(ad);
                if (metar.getPermissibleUsage().isPresent()) {
                    builder.setPermissibleUsage(metar.getPermissibleUsage());
                    if (metar.getPermissibleUsage().get().equals(PermissibleUsage.NON_OPERATIONAL)) {
                        builder.setPermissibleUsageReason(metar.getPermissibleUsageReason());
                    }
                } else {
                    builder.setPermissibleUsage(PermissibleUsage.OPERATIONAL);
                }
                ZonedDateTime reference_time = ZonedDateTime.now();
                METAR timeCorrectedMetar = METARImpl.Builder.from(builder.build()).withAllTimesComplete(reference_time).build();

                // METAR to IWXXM does not seem to be implemented currently
                System.out.println("Conversion of METAR TAC to POJO worked, to IWXXM unavailable");
                System.out.println("POJO:"+timeCorrectedMetar);

                // ConversionResult<String> iwxxmResult = metarAviMessageConverter.convertMessage(timeCorrectedTaf, IWXXMConverter.TAF_POJO_TO_IWXXM30_STRING, null);
                // if  (ConversionResult.Status.SUCCESS == iwxxmResult.getStatus()) {
                //     System.out.println(iwxxmResult.getConvertedMessage().get());
                // } else {
                //     System.out.println("IWXXM failed");
                //     System.err.println(iwxxmResult.getConversionIssues().size());
                //     for (ConversionIssue ci: iwxxmResult.getConversionIssues()) {
                //         System.out.println(ci);
                //     }
                // }
            } else {
                System.err.println("no pojo");
            }
        } else {
            System.err.println(result.getConversionIssues().size());
            for (ConversionIssue ci: result.getConversionIssues()) {
                System.out.println(ci);
            }
        }
    }

    private static Logger LOG = LoggerFactory.getLogger(MetarDemo.class);


    public static void main(String[] args){
        LOG.info("START METAR");
        SpringApplication.run(MetarDemo.class, args);
    }


}