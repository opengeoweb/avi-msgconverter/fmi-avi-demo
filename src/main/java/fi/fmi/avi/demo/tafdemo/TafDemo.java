package fi.fmi.avi.demo.tafdemo;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.ZonedDateTime;
import java.util.Optional;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fi.fmi.avi.converter.AviMessageConverter;
import fi.fmi.avi.converter.ConversionIssue;
import fi.fmi.avi.converter.ConversionResult;
import fi.fmi.avi.converter.iwxxm.conf.IWXXMConverter;
import fi.fmi.avi.converter.tac.conf.TACConverter;
import fi.fmi.avi.demo.ConverterConfiguration;
import fi.fmi.avi.model.Aerodrome;
import fi.fmi.avi.model.AviationCodeListUser.PermissibleUsage;
import fi.fmi.avi.model.immutable.AerodromeImpl;
import fi.fmi.avi.model.immutable.CoordinateReferenceSystemImpl;
import fi.fmi.avi.model.immutable.ElevatedPointImpl;
import fi.fmi.avi.model.taf.TAF;
import fi.fmi.avi.model.taf.immutable.TAFImpl;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(ConverterConfiguration.class)
class TafDemo implements CommandLineRunner {
    @Autowired
    AviMessageConverter tafAviMessageConverter;

    public void run(String ...args) {
        System.out.println("RUN TAF");
        convertTafToIwxxm();
    }

    public void convertTafToIwxxm() {
        String fn = "taf.tac";
        String input="";
        try {
            input = new String(Files.readAllBytes(Paths.get(fn)));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        System.out.println("input: ["+ input+"]");

        ConversionResult<TAF> result = tafAviMessageConverter.convertMessage(input, TACConverter.TAC_TO_TAF_POJO);
        System.out.println("ran TAC "+result.getStatus());
        if  (ConversionResult.Status.SUCCESS == result.getStatus()) {
            Optional<TAF> pojo = result.getConvertedMessage();
            if (pojo.isPresent()) {
                TAF taf = pojo.get();
                TAFImpl.Builder builder = TAFImpl.immutableCopyOf(taf).toBuilder();
                Aerodrome ad = AerodromeImpl.Builder.from(taf.getAerodrome())
                .setName("airport name")
                .setReferencePoint(ElevatedPointImpl.builder()
                        .setCrs(CoordinateReferenceSystemImpl.wgs84())
                        .addCoordinates(24.8325, 59.413333)
                        .setElevationUom("M")
                        .setElevationValue(999.)
                        .setVerticalDatum("EGM_96")
                        .build())
                .setLocationIndicatorICAO("EHXX")
                .build();
                builder = builder.setAerodrome(ad);
                if (taf.getPermissibleUsage().isPresent()) {
                    builder.setPermissibleUsage(taf.getPermissibleUsage());
                    if (taf.getPermissibleUsage().get().equals(PermissibleUsage.NON_OPERATIONAL)) {
                        builder.setPermissibleUsageReason(taf.getPermissibleUsageReason());
                    }
                } else {
                    builder.setPermissibleUsage(PermissibleUsage.OPERATIONAL);
                }
                ZonedDateTime reference_time = ZonedDateTime.now();
                TAF timeCorrectedTaf = TAFImpl.Builder.from(builder.build()).withAllTimesComplete(reference_time).build();


                ConversionResult<String> iwxxmResult = tafAviMessageConverter.convertMessage(timeCorrectedTaf, IWXXMConverter.TAF_POJO_TO_IWXXM30_STRING, null);
                if  (ConversionResult.Status.SUCCESS == iwxxmResult.getStatus()) {
                    System.out.println(iwxxmResult.getConvertedMessage().get());
                } else {
                    System.out.println("IWXXM failed");
                    System.err.println(iwxxmResult.getConversionIssues().size());
                    for (ConversionIssue ci: iwxxmResult.getConversionIssues()) {
                        System.out.println(ci);
                    }
                }
            } else {
                System.err.println("no pojo");
            }
        } else {
            System.err.println(result.getConversionIssues().size());
            for (ConversionIssue ci: result.getConversionIssues()) {
                System.out.println(ci);
            }
        }
    }

    private static Logger LOG = LoggerFactory.getLogger(TafDemo.class);


    public static void main(String[] args){
        LOG.info("START TAF");
        SpringApplication.run(TafDemo.class, args);
    }


}